package core;

import java.io.*;
import java.util.zip.InflaterInputStream;

public class GitCommitObject extends GitObject
{

    public GitCommitObject(String repository, String fileHash)
    {
        super(repository, fileHash);
    }

    public String getTreeHash()
    {
        BufferedReader br = new BufferedReader(new StringReader(UTF8_content));
        try
        {
            String s = br.readLine();

            return s.split(" ")[2];
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return "";
    }

    public String getParentHash()
    {
        BufferedReader br = new BufferedReader(new StringReader(UTF8_content));
        try
        {
            br.readLine();

            //Siamo sempre interessati solo alla seconda riga, il primo parent
            String s = br.readLine();

            return s.split(" ")[1];
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return "";
    }

    public String getAuthor()
    {
        BufferedReader br = new BufferedReader(new StringReader(UTF8_content));
        try
        {
            br.readLine();
            br.readLine();
            //Siamo sempre interessati solo alla terza riga
            //NB: si sta assumendo che ci sia un solo parent
            String s = br.readLine();

            return s.substring(s.indexOf("author ") + 7, s.indexOf(">")+1);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return "";
    }
}
