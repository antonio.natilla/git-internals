package core;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.zip.InflaterInputStream;

public class GitObject
{
    protected String hash;
    protected String type;
    protected String repoPath;

    protected String UTF8_content;

    public GitObject(String repositoryPath, String name)
    {
        this.repoPath = repositoryPath;
        this.hash = name;

        this.UTF8_content = getObjectAsString();
        this.type = UTF8_content.substring(0, UTF8_content.indexOf(" "));
    }

    public String getHash()
    {
        return this.hash;
    }

    public String getType()
    {
        return this.type;
    }


    public String getObjectAsString()
    {
        String readPath = this.repoPath+"/objects/"
                +this.hash.substring(0, 2)+"/"+this.hash.substring(2);

        try
        {
            InputStream input = new FileInputStream(new File(readPath));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            InflaterInputStream decompresser = new InflaterInputStream(input);
            byte[] buffer = new byte[1];
            int len;

            while((len = decompresser.read(buffer)) != -1)
                output.write(buffer, 0, len);

            return output.toString("UTF-8");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return "";
    }
}
