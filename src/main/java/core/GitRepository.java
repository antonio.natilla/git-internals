package core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository
{
	private String path;
	
	public GitRepository(String repo)
	{
		this.path = repo;
	}
	
	public String getHeadRef()
	{
		File headFile = new File(this.path + "/HEAD");

		try
		{
			BufferedReader br = new BufferedReader(new FileReader(headFile));
			return br.readLine().substring(5);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}


	public String getRefHash(String s)
	{
		try
		{
			return new BufferedReader(new FileReader(new File(this.path+"/"+s))).readLine();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return "";
		}
	}
}