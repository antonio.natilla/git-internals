package core;

import java.io.*;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class GitBlobObject extends GitObject
{

    public GitBlobObject(String repoPath, String fileName)
    {
        super(repoPath, fileName);
    }

    public String getType()
    {
        return this.type;
    }

    public String getContent()
    {
        return this.UTF8_content.substring(this.UTF8_content.indexOf('\0')+1);
    }
}
