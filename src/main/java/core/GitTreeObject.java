package core;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.InflaterInputStream;

public class GitTreeObject extends GitObject
{
    private HashMap<String, GitObject> tree;

    public GitTreeObject(String path, String name) throws IOException
    {
        super(path, name);

        tree = new HashMap<>();
        String content = UTF8_content.substring(UTF8_content.indexOf("\0")+1);
        System.out.println(content);

        String path_entry, path_hash;
        byte[] bcontent;

        String readPath = this.repoPath+"/objects/"
                + this.hash.substring(0, 2)+ "/" + this.hash.substring(2);

        try
        {
            InputStream input = new FileInputStream(new File(readPath));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            InflaterInputStream decompresser = new InflaterInputStream(input);
            byte[] buffer = new byte[1];
            int len;

            while((len = decompresser.read(buffer)) != -1)
                output.write(buffer, 0, len);

            bcontent  = output.toByteArray();



            int i = 0;
            int j;

            while(i < bcontent.length && (char) bcontent[i] != '\0')
                i++;

            for(i = i+2; i < bcontent.length; i++)
            {
                path_entry = path_hash = "";

                while((char) bcontent[i] != ' ')
                    i++;

                for(j = i + 1; bcontent[j] != '\0'; j++)
                {
                    path_entry += (char) bcontent[j];
                }
                i = j;

                System.out.println(path_entry);

                i += 1;

                byte b20[] = new byte[20];
                for(j = i; j < i + 20; j++)
                {
                    b20[j-i] = bcontent[j];
                }
                i = j;

                path_hash = bytesToHexHash(b20);

                System.out.println(path_hash);

                System.out.println(new GitObject(path, path_hash).getType());
                if(new GitObject(path, path_hash).getType().equals("tree"))
                    tree.put(path_entry, new GitTreeObject(path, path_hash));
                else if (new GitObject(path, path_hash).getType().equals("blob"))
                    tree.put(path_entry, new GitBlobObject(path, path_hash));
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }



        return;
        /*
        StringReader sr = new StringReader(content);

        char[] buffer = new char[20];
        int offset = 0;
        while(sr.read(buffer, 0, 6) != -1)
        {
            offset += 6;
            sr.read(buffer, 0, 1);
            path_entry = path_hash = "";
            while(sr.read(buffer, 0, 1) != -1 && buffer[0] != '\0')
            {
                path_entry += buffer[0];
                offset++;
            }
            sr.read(buffer, 0, 20);

            path_hash = new String(buffer);

            for(int i = 0; i < buffer.length; i++)
            {

                path_hash += Integer.toHexString((byte)buffer[i]);
            }

            System.out.println(path_hash);
            System.out.println(path_entry);
            tree.put(path_entry, new GitObject(path, path_hash));
        }
        */


    }

    public ArrayList<String> getEntryPaths()
    {
        return new ArrayList<String>(tree.keySet());
    }

    public String getContent()
    {
        return this.UTF8_content;
    }

    public GitObject getEntry(String entry_path)
    {
        return tree.getOrDefault(entry_path, null);
    }

    public String bytesToHexHash(byte[] b)
    {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[b.length * 2];
        int c;
        for(int j=0; j < b.length; j++)
        {
            c = b[j] & 0xFF;
            hexChars[2*j] = hexArray[c >>> 4];
            hexChars[2*j + 1] = hexArray[c & 0x0F];
        }

        return new String(hexChars).toLowerCase();
    }

}
